using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Moving : MonoBehaviour
{

    public float speed =  10f;

    public Rigidbody m_Rigidbody;
    public float thrust = 10f;
    public float JumpHeight = 0f;
    public float jumpSpeed = 0.3f;
    public float jumpMax = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        this.m_Rigidbody = transform.parent. GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        this.MoveByRigidbody();
    }

    // Update is called once per frame
    protected void Update()
    {
        this.Jumping();
    }

    protected virtual void MoveByRigidbody()
    {
        float x = InputManager.Instance.vertical;
        float z = -1 * InputManager.Instance.horizontal;
        float y = InputManager.Instance.Jump;

        Vector3 direction = new Vector3(x, this.JumpHeight, z);
        this.m_Rigidbody.AddForce(direction * this.thrust);
    }

    protected virtual void Jumping()
    {
        if(InputManager.Instance.Jump == 0)
        {
            return;
        }
        this.JumpHeight += this.jumpSpeed;
        Invoke("ResetJump", this.jumpMax);
    }

    protected virtual void ResetJump()
    {
        this.JumpHeight = 0f;
    }
}
