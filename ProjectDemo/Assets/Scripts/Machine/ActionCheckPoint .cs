using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCheckPoint : Action
{
    public MachineController machine;

    private void Start()
    {
        this.machine = transform.parent.GetComponent<MachineController>();
    }
    public override void Act()
    {
        this.SaveCheckPoint();
    }

    protected virtual void SaveCheckPoint()
    {
        Debug.Log(transform.name + ":save checkpoint");
        CheckPointManager.instance.SetCheckPoints(this.machine);
    }

    
}
