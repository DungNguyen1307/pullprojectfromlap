using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform Player;
    public Vector3 offset = new Vector3(-5, 5, 0);
    // Start is called before the first frame update
    void Start()
    {
        this.Player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = this.Player.transform.position + this.offset;
        transform.LookAt(this.Player.position);
    }
}
