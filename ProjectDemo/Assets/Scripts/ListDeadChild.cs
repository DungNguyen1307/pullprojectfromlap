using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class ListDeadChild : Parent 
{
// Start is called before the first frame update

    protected override void ShowAllChildren()
    {
        foreach (Transform Child in transform) 
        {
            if (this.IsDead(Child))
            {
                Debug.Log(Child.name);
            }
        }
    }

    protected virtual bool IsDead(Transform monster)
    {
        if (monster.name.Contains("Dead")) return true;
        return false;
    }

    // Update is called once per frame
}
