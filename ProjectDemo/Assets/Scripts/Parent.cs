using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.ShowAllChildren();
    }

    protected virtual void ShowAllChildren()
    {
        foreach (Transform child in transform)
        {
            Debug.Log(child.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
