using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    public static CheckPointManager instance;
    public List<MachineController> CheckPoints;
    public MachineController currentCheckPoints;
    // Start is called before the first frame update
    private void Awake()
    {
        CheckPointManager.instance = this;
    }

    void Start()
    {
        this.LoadCheckPoints();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected virtual void LoadCheckPoints()
    {
        MachineController machineCtrl;
        foreach(Transform child in transform)
        {
            machineCtrl = child.GetComponent<MachineController>();
            this.CheckPoints.Add(machineCtrl);
        }
    }

    public virtual void SetCheckPoints(MachineController CheckPoint)
    {
        if(this.IsOldCheckPoint(H))
        this.currentCheckPoints = CheckPoint;
    }
}
