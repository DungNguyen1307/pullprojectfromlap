using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDisable : Action
{
    public float respawnDelay = 3f;
    public float DisableDelay = 0.3f;

    public override void Act()
    {

        Invoke("Disable", this.DisableDelay);

        Invoke("Respawn", this.respawnDelay);
    }

    protected virtual void Disable()
    {
        transform.parent.gameObject.SetActive(false);
    }

    protected virtual void Respawn()
    {
        transform.parent.gameObject.SetActive(true); 
    }
    
}
