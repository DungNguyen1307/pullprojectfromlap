using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public MachineController machine;

    private void Start()
    {
        this.machine = transform.parent.GetComponent<MachineController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collider:" + other.name);
        Debug.Log(transform.name);

        this.machine.action.Act();  
    }

}
