using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    public float vertical = 0;
    public float horizontal = 0;
    public float Jump = 0;

    private void Awake()
    {
        InputManager.Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetInputMovement();
    }

    protected virtual void GetInputMovement()
    {
        this.vertical = Input.GetAxisRaw("Vertical");
        this.horizontal = Input.GetAxisRaw("Horizontal");
        this.Jump = Input.GetAxisRaw("Jump");
    }

}
