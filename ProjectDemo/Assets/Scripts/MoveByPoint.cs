using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEditor;
using UnityEngine;

public class MoveByPoint : MonoBehaviour
{
    public List<Transform> points;
    public Transform pointsHolder;
    public int PointIndex = 0;
    public float Speed = 0.05f;
    public double pointDistance = Mathf.Infinity;

    private void Start()
    {
        this.LoadPoint();
    }

    private void Update()
    {
        this.PointMoving();
        
    }

    private void FixedUpdate()
    {
        this.NextPointsMove();
    }

    protected virtual void LoadPoint()
    {
        string name = transform.name + "_Point";
        this.pointsHolder = GameObject.Find(name).transform;
        foreach(Transform point in this.pointsHolder)
        {
            this.points.Add(point);
        }
    }

    //protected virtual void PointLerpMoving()
    //{
    //    float lerp = 0.1f * Time.deltaTime;
    //    Transform currentPoins = this.points[this.PointIndex];
    //    transform.position = Vector3.Lerp(transform.position, currentPoins.position, lerp);
    //}

    protected virtual void PointMoving()
    {
        float step = this.Speed;
        Transform currentPoins = this.CurrentPoint();
        transform.position = Vector3.MoveTowards(transform.position, currentPoins.position, step);
    }

    protected virtual void NextPointsMove()
    {
        this.pointDistance = Vector3.Distance(transform.position, this.CurrentPoint().position);
        if (this.pointDistance == 0)
        {
            this.PointIndex++;
        }
        if(this.PointIndex >= this.points.Count)
        {
            this.PointIndex = 0;
        }
    } 

    public virtual Transform CurrentPoint()
    {
       return this.points[this.PointIndex];
    }
}
